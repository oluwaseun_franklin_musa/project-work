
%%%%%%%%%%%%%%%% BUILDING THE VECTOR OF BOUNDARY CONDITIONS FOR THE CRANK NICOLSON METHOD %%%%%%%%%%%%%%%%  

function F=bounds(Smax,sigmax,M,N,theta,r,nu,kappa,K,t)


h=Smax/(M+1);  l=sigmax/(N+1);

S=zeros(M+1,1);   sig=zeros(N+1,1);   a=zeros(M,N); b=zeros(M,N); c=zeros(M,N); d=zeros(N,1); e=zeros(N,1);

A=theta^2/(2*l^2); B=-theta^2/(l^2)+kappa*nu/l; C=zeros(M,1); D=theta^2/(2*(l^2))-kappa*nu/l-r; E=zeros(M,1);


for i=1:M+1
    
    S(i)=i*h;
    
end

    for j=1:N;
        
        d(j)=0.5*theta^2/l^2-kappa*(nu-sig(j))/(2*l);
        
    end
    

for i=1:M
    
    C(i)=r*S(i)/(2*h);
    
end

for i=1:M
    
    for j=1:N
        
        a(i,j)=(sig(j)^2*S(i)^2)/(2*h^2)+(r*S(i))/(2*h);
        
        
    end
end


%%%%%%%%%%%%%%%%%%%%%% THE VECTOR OF BOUNDARY CONDITIONS %%%%%%%%%%%%%%%%%%%%%

F=zeros(M*(N+1),1);                           % Initializing the vector of boundary conditions

for i=1:M-1

        F(N*i,1)=d(N)*S(i); 
        
end

for i=((M-1)*N)+1:M*N-1;
    
        F(i,1)=h*a(M,1);
        
end
    
        
F(M*N,1)=a(M,N)*h+d(N)*S(M);

F(M*N+M,1)=C(M)*h;

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%












