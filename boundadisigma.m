%%%%%%%%%%%%% Function for boundary condition for the volatility direction %%%%%%%%%%%%% 

function F2=boundadisigma(Smax,sigmax,M,N,kappa,nu,theta)

h=Smax/(M+1); l=sigmax/(N+1);

S=h:h:Smax;

sigma=l:l:sigmax;


F2=zeros(M*N+M,1);


A=zeros(N,1);


for j=1:N
    
    A(j)=1/2*theta^2/l^2+kappa*(nu-sigma(j))/(2*l);
    
end


for i=1:M
    
    F2(i*N,1)=A(N)*S(i);
    
end



spy(F2)
