%%%%%%%%%%%%%%%  Function for the boundary condition in the stock price direction %%%%%%%%%%%%% 

function F1=boundadistock(Smax,sigmax,M,N,r)

h=Smax/(M+1); l=sigmax/(N+1);  d=zeros(M,1)


S=h:h:Smax;  sigma=l:l:sigmax;


a=zeros(M,N);

F1=zeros(M*N+M,1);


for i=1:M
    
    for j=1:N
        
        a(i,j)= 1/2*sigma(j)^2*S(i)^2/h^2+r*S(i)/(2*h);
        
    end
    
end

 
for i=1:M
    
    d(i)=r*S(i)/(2*h);
    
end


 for j=1:N
     
     F1((M-1)*N+j,1)=a(M,j)*h;
     
 end

F1(M*N+M,1)=d(M)*h;

end
