%%%%%%% THE RESULT IS GENERATED HERE %%%%%%%

M=30; N=20;  r=0.953;  K=100;  

Smax=200; sigmax=1; h=Smax/(M+1);  l=sigmax/(N+1);
tita=1.0;



Z1=adistock(Smax,sigmax,M,N,r);

P=adisigma(Smax,sigmax,M,N,kappa,nu,theta,r);                                     

F1=boundadistock(Smax,sigmax,M,N,r);


F2=boundadisigma(Smax,sigmax,M,N,kappa,nu,theta);

S=h:h:Smax;  

sigma=l:l:sigmax;

%for i=1:M
    
    %d(i)=r*S(i)/(2*h);
    
%end

V=zeros(M*N+M,1);                   

I=eye(M*N+M);                            % Identity matrix


% initial condition 

for i=1:M
    
    
    for j=1:N
        
        
        V((i-1)*N+j)=max(S(i)-K,0);% the Payoff function
        
    end
    
    
end 


%%%%%% IMPLEMENTATION OF ADI (DOUGLAS) ALGORITHM  %%%%%%

for t=dt:dt:T
    
    Y0=V+dt*((Z1+P)*V+(F1+F2));
    
    Y1=(I-tita*dt*Z1)\(Y0-tita*dt*Z1*V);
    
    Y2=(I-tita*dt*P)\(Y1-tita*dt*P*V);
    
    V=Y2;
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

V1=V(1:M*N);
Vp=reshape(V1,N,M);




surf(S(1:M),sigma(1:N),Vp);
xlabel('Stock Price');
ylabel('Volatility');
zlabel('European Call Option Price Using ADI');
set(gcf,'renderer','zbuffer');

%%% The result is a plot. In order to read the values of the option price that correspond to a stock price and a volatility value,   the data cursor button can be clicked and adjusted to the stock price and the volatility. The corresponding option price is displayed together with the stock price and volatility. This is not the only way of doing this; it can be done by interpolation.

%%% The parameters can be varied accordingly. 


