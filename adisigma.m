%%%%% MATRIX OF DERIVATIVES IN THE VOLATILITY DIRECTION %%%%%%%

function P=adisigma(Smax,sigmax,M,N,kappa,nu,theta,r)        % Function definition

S=zeros(M+1,1); sigma=zeros(N+1,1);

h=Smax/(M+1); l=sigmax/(N+1); B=-theta^2/l^2-r/2; D=1/2*theta^2/l^2;

E=-theta^2/l^2+kappa*nu/l; F=1/2*theta^2/l^2-kappa*nu/l-r/2;

P=zeros(M*N+M,M*N+M);

A=zeros(N,1); C=zeros(N,1); 

%%%%%% Vector of values of the stock price on the grid %%%%%%%

for i=1:M+1
    
    S(i)=i*h;
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%% Vector of values of the volatility on the grid %%%%%%%%

for j=1:N+1
    
    sigma(j)=j*l;
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 


%%%%% Vector of coefficients %%%%

for j=1:N
    
    A(j)=1/2*theta^2/l^2+kappa*(nu-sigma(j))/(2*l);
    
end

for j=1:N
    
    C(j)= 1/2*theta^2/l^2-kappa*(nu-sigma(j))/(2*l);
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%% Building the matrix %%%%
for i=1:M
      
      for j=1:N
          
          P((i-1)*N+j,(i-1)*N+j)=B;
          
          
      end
      
 end


for i=1:M
      
      for j=1:N-1
          
          P((i-1)*N+j,(i-1)*N+j+1)=A(j);
                   
      end
      
end


for i=1:M
      
      for j=2:N
          
          P((i-1)*N+j,(i-1)*N+j-1)=C(j);
          
          
      end
      
end

for i=1:M
    
    P((i-1)*N+1,M*N+i)=C(1);
    
end

for i=1:M
    
    P(M*N+i,(i-1)*N+1)=E;
    
end


for i=1:M
    
    P(M*N+i,(i-1)*N+2)=D;
    
end

for i=1:M
    
    P(M*N+i,M*N+i)=F;
    
end

%%%%%%%%%%%%%%%%%%%%%%%
