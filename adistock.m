%%%%%%%% MATRIX OF DERIVATIVES IN THE STOCK PRICE DIRECTION %%%%%%%

function Z1=adistock(Smax,sigmax,M,N,r)          % Function definition

S=zeros(M+1,1); sigma=zeros(N+1,1); 

h=Smax/(M+1); l=sigmax/(N+1); e=-r/2;

Z1=zeros(M*N+M,M*N+M);

a=zeros(M,N); b=zeros(M,N); c=zeros(M,N); d=zeros(M,1); f=zeros(M,1);


%%%%%%% Vector of values of stock price on the grid %%%%%%%%

for i=1:M+1
    
    S(i)=i*h;
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%% Vector of values of volatility on the grid %%%%%%%%%

for j=1:N+1
    
    sigma(j)=j*l;
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 


%%%%%%%%%%% Building matrices of coefficients %%%%%%%%%%%%%%%
for i=1:M
    
    for j=1:N
        
        a(i,j)= 1/2*sigma(j)^2*S(i)^2/h^2+r*S(i)/(2*h);
        
    end
    
end

for i=1:M
    
    for j=1:N
        
        b(i,j)=-sigma(j)^2*S(i)^2/h^2-r/2;
        
    end
    
end

for i=1:M
    
    for j=1:N
        
        c(i,j)=1/2*sigma(j)^2*S(i)^2/h^2-r*S(i)/(2*h);
        
    end
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%% Vectors of coefficients %%%%%%%%%%%%%%%%%%%%%

for i=1:M
    
    d(i)=r*S(i)/(2*h);
    
end

for i=1:M
    
    f(i)=-r*S(i)/(2*h);
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  


%%%%%%%%%%%%%%% Building the matrix of derivatives in the stock price direction %%%%%%%%%%%%%%

  for i=1:M
      
      for j=1:N
          
          Z1((i-1)*N+j,(i-1)*N+j)=b(i,j);
                 
      end
      
  end
  
  %spy(Z1)     % To see the shape of the entries of the matrix
  

  
  for i=1:M-1
      
      for j=1:N
      
      Z1((i-1)*N+j,(i*N+j))=a(i,j);
      
      end
  
  end
  
  %spy(Z1)
  
  
  for i=2:M
      
      for j=1:N
          
          Z1((i-1)*N+j,(i-2)*N+j)=c(i,j);
      
      end
      
  end
  
  
  for j=1:N
           
      Z1((M-1)*N+j,(M-1)*N+j)=a(M,j)+b(M,j);
      
  end
  
  %spy(Z1)
  
  %for j=1:N
      
      %Z1(M*N+j,(M-1)*N+j)=a(M,j);
      
  %end
  
  for i=1:M-1
      
      Z1(M*N+i,M*N+i)=e;
      
  end
  
  for i=1:M-1
      
      Z1(M*N+i,M*N+i+1)=d(i);
      
  end
  
  for i=2:M
      
      Z1(M*N+i,M*N+i-1)=f(i);
      
  end

Z1(M*N+M,M*N+M)=e+d(M);

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
