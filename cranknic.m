
%%%%% WEIGHTED METHOD (CRANK NICOLSON) %%%%% 

clear all;
clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% GRID SETTING  %%%%%%%%%%%

 % Smax is the maximum value of the stock price and sigmax the maximum value of volatility, T is the maximum time
Smax=200;  sigmax=1;  M=30;  N=20;  T=1/12; J=5;       
                                                                             
S=zeros(M+1,1);   sig=zeros(N+1,1);                     % Initialization for S stock price and sig represents the volatility
                                        
% h is the step size for stock price, l is the step size for volatility and dt is the step size for time            
h=Smax/(M+1);  l=sigmax/(N+1); dt=T/J;                

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%% PARAMETER DEFINITION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% r is the risk free rate, theta is the volatility of volatility, kappa is the mean reverting speed, nu is the mean reverting level

r=0.953; theta=0.4; kappa=4; nu=0.35; K=100;  

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%% COEFFICIENTS AND MATRIX BUILDING %%%%%%%%%%%%%%%%%%%%%%%%%%%%

a=zeros(M,N); b=zeros(M,N); c=zeros(M,N); d=zeros(N,1); e=zeros(N,1);     % Initialization

A=theta^2/(2*l^2); B=-theta^2/l^2+kappa*nu/l; C=zeros(M,1); D=theta^2/(2*l^2)-kappa*nu/l-r; E=zeros(M,1);

Z=zeros(M*N+M,M*N+M);     % Matrix Initialization

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%% VECTOR OF THE VALUES OF STOCK PRICE ON THE GRID %%%%%%%%%%%%%%

for i=1:M+1
    
    S(i)=i*h;
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%% VECTOR OF THE VALUES OF VOLATILITY ON THE GRID %%%%%%%%%%%

for j=1:N+1
    
    sig(j)=j*l;
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%% MATRICES OF COEFFICIENTS %%%%%%%%%%%%%%%%%
for i=1:M
    
    for j=1:N
        
        a(i,j)=(sig(j)^2*S(i)^2)/(2*h^2)+(r*S(i))/(2*h);
        
        
    end
end
        
 

for i=1:M
    
    for j=1:N
        
        b(i,j)=-((sig(j)^2*S(i)^2)/(h^2))-((theta^2)/(l^2))-r;
        
    end
    
end


for i=2:M
    
    for j=1:N
        
        c(i,j)=(sig(j)^2*S(i)^2)/(2*h^2)-(r*S(i))/(2*h);
        
    end
    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    
%%%%%%%%%%%%%%%%%%%%%%%% VECTOR OF COEFFICIENTS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    for j=1:N;
        
        d(j)=((theta^2)/(2*l^2))+(kappa*(nu-sig(j)))/(2*l);
        
    end
    


    for j=1:N
        
        e(j)=(theta^2)/(2*l^2)-(kappa*(nu-sig(j)))/(2*l);
        
    end
    


    for i=1:M
    
        C(i)=r*S(i)/(2*h);
    
    end



    for i=2:M
    
        E(i)=-r*S(i)/(2*h);
    
    end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%% BUILDING THE MATRIX %%%%%%%%%%%%%%%%%%%%%

Seun= zeros(M,N);

    for j=1:N;
        
        Seun(M,j)=a(M,j);

    end
        
P1=[];

for i=1:M
    
    Ps=diag(b(i,1:N))+diag(Seun(i,1:N))+diag(d(1:N-1),1)+diag(e(2:N),-1);
    
    P1=blkdiag(P1,Ps);
    
end


%spy(P1);                                   THIS SHOWS THE STRUCTURE OF THE MATRIX

P2=[];

for i=1:M-1
    
    Pt=diag(a(i,1:N));
    
    P2=blkdiag(P2,Pt);
    
end


%size(P2)                                   TO CHECK THE DIMENSION OF THE MATRIX

P3=[];

for i=2:M
    
    Pq=diag(c(i,1:N));
    
    P3=blkdiag(P3,Pq);
    
end


P=P1+[[zeros((M-1)*N,N),P2];zeros(N,M*N)]+[zeros(N,M*N);[P3,zeros((M-1)*N,N)]];


spy(P)


for i=1:M
    
    Z(M*N+i,(i-1)*N+1)=B;
    
    Z(M*N+i,(i-1)*N+2)=A;
    
end

for i=1:M
    
    Z((i-1)*N+1,M*N+i)=e(1);
    
end



for i=1:M-1
    
    Z(M*N+i,M*N+i)=D;
    
end

Z(M*N+M,M*N+M)=D+C(M);

for i=1:M-1
    
    Z(M*N+i,M*N+i+1)=C(i);
    
end

for i=2:M
    
    Z(M*N+i,M*N+i-1)=E(i);
    
end


Z(1:M*N,1:M*N)=P;                       % THIS IS THE MAIN MATRIX

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% spy(Z)         






%%%%%   Initial condition   %%%%%

Vstar=zeros(M*N+M,1);
for i=1:M
    for j=1:N
        Vstar((i-1)*N+j)=max(S(i)-K,0);% the Payoff function
    end
end 

V1=Vstar(1:M*N);
V=reshape(V1,N,M);




surf(S(1:M),sig(1:N),V);
xlabel('Stock Price');
ylabel('Volatility');
zlabel('European Call Option Price Using Crank-Nicolson Method');



tita=0.5;

 for t=dt:dt:T
     I=speye(M*N+M,M*N+M);

     G=(I-(tita*dt*Z));
     H=(I+((1-tita)*dt*Z));
     F=bounds(Smax,sigmax,M,N,theta,r,nu,kappa,K,t);
     GI=bounds(Smax,sigmax,M,N,theta,r,nu,kappa,K,t+dt);
     G2=(tita*dt*GI)+((1-tita)*dt*F);
     YY=G\(H*Vstar+G2);
     Vstar =YY;

 end


V1=Vstar(1:M*N);
V=reshape(V1,N,M);




surf(S(1:M),sig(1:N),V);
xlabel('Stock Price');
ylabel('Volatility');
zlabel('European Call Option Price Using Crank-Nicolson Method');


%%% The result is a plot. In order to read the values of the option price that correspond to a stock price and a volatility value,   the data cursor button can be clicked and adjusted to the stock price and the volatility. The corresponding option price is displayed together with the stock price and volatility. This is not the only way of doing this; it can be done by interpolation.

%%% The parameters can be varied accordingly.

